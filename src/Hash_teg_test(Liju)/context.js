import React,{useState,createContext} from 'react'

export const FanconvoContext = createContext(  )

export function FanconvoContextProvider(props) {

    const initial_form_data = {
        first_name:"",
        last_name:"",
        username:"",
        Email:"",
        Timezone:"",
        password:""

    }
    const [fansignup,setFansignup]=useState(true)
    const [formData,setFormdata] = useState(initial_form_data)
    

    return (
        <FanconvoContext.Provider value={{value_1:[formData,setFormdata],value_2:[fansignup,setFansignup]}}>
            {props.children}
        </FanconvoContext. Provider>
       
    )
}