import React from 'react'
import Nav from './sign_up_componenets/nav'
import Form from './sign_up_componenets/form'
import './sign_up_componenets/App.css'
function Main() {
    return (
        <div className="App">
            <Nav/>
            <Form/>
        </div>
    )
}

export default Main
