import React,{useContext} from 'react'
import './App.css'
import {API} from '../api/baseUrl'
import {FanconvoContext} from '../context'

function Form() {
  const {value_1,value_2}= useContext(FanconvoContext)

    const [formData,setFormdata] = value_1
    const [fansignup,setFansignup]=value_2

    const handleChanage = (e)=>{
        setFormdata({...formData,[e.target.name]:e.target.value})

    }
    const handleSubmit=(e)=>{
        e.preventDefault();
         fansignup?API.post('/talent',formData).then(res=>console.log(res)).catch(error=>console.log(error)):API.post('/fan',formData).then(res=>console.log(res)).catch(error=>console.log(error))
        
        
    }
    return (
<div>
            
<body>
    {console.log(fansignup)}
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <nav>
            <ul className="navLinks_Buttons">
              <li><button  onClick={()=>setFansignup(true)} className="btn btn-success " >Fan Singn_up</button></li>
              <li></li>
              <li><button onClick={(e)=>setFansignup(false)} className="btn btn-success ">talent Singn_up</button></li>
            </ul>
            </nav>
           
          
            <h5 class="card-title text-center">Create Your Fan Account</h5>
            <form onSubmit={handleSubmit} class="form-signin">
              <div class="form-label-group">
                <label>First name *</label>
                <br/>
                <br/>
                <br/>
                <input onChange={handleChanage} name="first_name" type="text" id="inputEmail" className="form-control" placeholder="First_Name" required autofocus/>
                
              </div>
              <div class="form-label-group">
                <label>Last name *</label>
                <br/>
                <br/>
                <br/>
                <input name="last_name" onChange={handleChanage} type="text" id="inputEmail" className="form-control" placeholder="First_Name" required autofocus/>
                
              </div>
              <div class="form-label-group">
                <label>username *</label>
                <br/>
                <br/>
                <br/>
                <input name="username"onChange={handleChanage} type="text" id="inputEmail" className="form-control" placeholder="First_Name" required autofocus/>
                
              </div>
              <div class="form-label-group">
                <label>Email *</label>
                <br/>
                <br/>
                <br/>
                <input name="Email" onChange={handleChanage} type="email" id="inputEmail" className="form-control" placeholder="First_Name" required autofocus/>
                
              </div>
              <div class="form-label-group">
                <label>Timezone *</label>
                <br/>
                <br/>
                <br/>
                {/* <input name="timezone" type="text" id="inputEmail" className="form-control" placeholder="First_Name" required autofocus/> */}
                <select name="Timezone" onChange={handleChanage} autofocus className="form-control">
                    <option>{Date.now()}</option>
                    <option>{Date.now()}</option>

                </select>
                
              </div>
              <div class="form-label-group">
                <label>Password *</label>
                <br/>
                <br/>
                <br/>
                <input name="password" onChange={handleChanage} type="password" id="inputEmail" className="form-control" placeholder="First_Name" required autofocus/>
                
              </div>

              <div className="custom-control custom-checkbox mb-3">
                <input type="checkbox" className="custom-control-input" id="customCheck1"/>
                <label className="custom-control-label" for="customCheck1">Remember password</label>
              </div>
              <button className="btn btn-lg btn-success btn-block text-uppercase" type="submit">Sign in</button>
              <hr className="my-4"/>
              
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

           
        </div>
    )
}

export default Form
