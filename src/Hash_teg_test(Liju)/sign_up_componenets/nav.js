import React from 'react'
import image_1 from '../images/Fanconvo-header-logo (3) (1).png'
import './App.css'

function Nav() {
    const style = {
        width:'20%',
        height:'auto'
    }
    return (
        <div>
            <nav>
                <img style={style} src={image_1}/>
               
                <ul className="navLinks">
                    <li>Sign Up</li>
                    <li>Login</li>
                </ul>
                
                
            </nav>
            
        </div>
    )
}

export default Nav
